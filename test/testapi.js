const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

//para hacer las aserciones
var should = chai.should();

//should, expect, assert
//definir agrupación de test function
//it: caso particular a probar
//done, función manejadora
describe("Test de API usuarios",
  function(){
    it("Prueba de que la API responde", function(done){
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/user')
        .end(
          function(err, res){
            console.log("Request finished");
            //console.log(res);
            //console.log(err);
            res.should.have.status(200);
            res.body.key.should.be.eql("Hola");
            done();
          }
        )
    }),
    it(
      "Prueba de que la API devuelve la lista de users", function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
            function(err, res){
              console.log("Request finished");
              //console.log(res);
              //console.log(err);
              res.should.have.status(200);
              res.body.should.be.a("array");

              for (user of res.body){
                user.should.have.property('first_name');
                user.should.have.property('email');

              }

              done();
            }
          )
      }

    )

  }
)
