#Dockerfile

#Imagen raiz
FROM node

#Carpeta raiz
WORKDIR /apitechu

#Copia de archivos de local a Imagen
# (. el directorio donde estoy lanzando el build)
ADD . /apitechu

#Instalación de dependencias
#comandos en tiempo de construcción BUILD
RUN npm install --only=prod

#Puerto que vamos a usuario
#(puerto del contenedor y el de la máquina donde corre el contenedor)
EXPOSE 3000

#Comando de inicialización
#CMD en tiempo de levantar el contenedor (docker run)
CMD ["node", "server.js"]
