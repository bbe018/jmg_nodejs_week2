console.log("Hola Mundo")
// FRAMEWORK express
const express = require('express');

const app = express();
app.use(express.json());



const io=require('./io');
const env=require('dotenv').config();
const userController = require('./controllers/UserController');
const MovController = require('./controllers/MovController');
const AuthController = require('./controllers/AuthController');
const AccountController = require('./controllers/AccountController');


var enableCORS= function (req,res,next){
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");
  next();
}
app.use(enableCORS);
const port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto CAMBIO"+ port)

//Comprueba login
app.post("/apitechu/v1/login",AuthController.loginCheck);
app.post("/apitechu/v2/login",AuthController.loginCheckv2);

//Haz logout
app.post("/apitechu/v1/logout/:id",AuthController.logout);
app.post("/apitechu/v2/logout/:id",AuthController.logoutv2);

//Respuesta ante una invocación GET externa REST
app.get("/apitechu/v1/user",userController.getUser);
//Respuesta ante una invocación GET externa REST
app.get("/apitechu/v1/users",userController.getUsersV1);
//Respuesta ante una invocación GET externa REST
app.get("/apitechu/v2/users",userController.getUsersV2);
//Respuesta ante una invocación GET externa REST
app.get("/apitechu/v2/users/:id",userController.getUsersByIdV2);
//Respuesta ante una invocación GET externa REST
app.get("/apitechu/movs/:account",MovController.getMovsV1);
//Respuesta ante una invocación GET externa REST
app.get("/apitechu/movs/",MovController.getMovsV2);
//Respuesta ante una invocación GET externa REST
app.get("/apitechu/v1/accounts/:id",AccountController.getAccountById);

//POST
app.post("/apitechu/v1/users",userController.createUserv1);
app.post("/apitechu/v2/users",userController.createUserv2);
app.post("/apitechu/movs",MovController.createMovsv1);
app.post("/apitechu/update/:saldo/:account",AccountController.BalanceUpdate);
app.post("/apitechu/update/",AccountController.BalanceUpdatev2);

//DELETE ARRAY FOR ELEMENT IN OBJECT by id
app.delete("/apitechu/v1/users/:id",userController.deleteUserv1)
app.delete("/apitechu/v2/users/:id",userController.deleteUserv2)

//USO DE FILTROS:
app.get("/apitechu/v1/users_topcount",
  function(req, res){
    var users=require('./MOCK_DATA.json');
    len_array=users.length;

    console.log("top: "+req.query.$TOP);
    console.log("count: "+req.query.$COUNT);
    console.log("json count: "+len_array);

    top=  req.query.$TOP;
    count= req.query.$COUNT;

      if (top > 0){
        result=users.slice(0,req.query.$TOP)

      }else {
        result=users
      }
    console.log(result);

    if (count == "TRUE"){
      res_count=top
      var OB={ "count": len_array, "users": result}
    }else {
      res_count=len_array
      var OB={"users": result}

    }
    console.log(res_count);

    res.send(OB);
    }
  )







//BORRADO POR ID (ARRAY FIND INDEX)
/*
app.delete("/apitechu/v1/users/:id",
  function(req, res){
      console.log("DELETE /apitechu/v1/users");
      console.log("Id usuario a borrar es: "+req.params.id);
      var users=require('./usuarios.json');

      index=users.findIndex(function isDeletedIndex(element) {
        return element.id == req.params.id;
      });

      users.splice(index,1)

      console.log(users)
      io.writeUserDataToFile(users);
      console.log("Proceso de borrado de usuario terminado");
      res.send({"msg": "usuario borrado"});
  }

)
//DELETE FOR ELEMENT IN ITERABLE
app.delete("/apitechu/v1/users/:id",
  function(req, res){
      console.log("DELETE /apitechu/v1/users");
      console.log("Id usuario a borrar es: "+req.params.id);
      var users=require('./usuarios.json');
      var index=0
      for (const value of users) {

        if (value.id == req.params.id){
          console.log("Entra")
          console.log(index);
          users.splice(index,1)
        }
        index= index +1;
      }

      console.log(users)
      io.writeUserDataToFile(users);
      console.log("Proceso de borrado de usuario terminado");
      res.send({"msg": "usuario borrado"});
  }

)

//DELETE ARRAY FOR EACH
app.delete("/apitechu/v1/users/:id",
  function(req, res){
      console.log("DELETE /apitechu/v1/users");
      console.log("Id usuario a borrar es: "+req.params.id);
      var users=require('./usuarios.json');

      var index=0
      users.forEach(function (item) {
        if (item.id == req.params.id){
          console.log("Borra index: "+ index)
          users.splice(index,1)
        }
        index= index +1;
        console.log(item.id);
      });

      console.log(users)
      io.writeUserDataToFile(users);
      console.log("Proceso de borrado de usuario terminado");
      res.send({"msg": "usuario borrado"});
  }

)

//DELETE ARRAY FOR by id
app.delete("/apitechu/v1/users/:id",
  function(req, res){
      console.log("DELETE /apitechu/v1/users");
      console.log("Id usuario a borrar es: "+req.params.id);
      var users=require('./usuarios.json');
      var index=0
      for (var i = 0, len = users.length; i < len; i++) {
        item=users[i];
        if (item.id == req.params.id){
          index=i;
          console.log("Borra index: "+ i);
          //users.splice(i,1)
        }

      }

      users.splice(index,1)
      console.log(users)
      io.writeUserDataToFile(users);
      console.log("Proceso de borrado de usuario terminado");
      res.send({"msg": "usuario borrado"});
  }

)*/
