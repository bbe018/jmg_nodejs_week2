
const io=require('../io');
const crypt=require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmg11ed/collections/";
//const mLabAPIKey = "apiKey=5zTFmFoXB8txjvfwTLCXt6mCqPL-P_ea";

const mLabAPIKey="apiKey="+ process.env.MLAB_API_KEY


function BalanceUpdate(req, res) {
//req y res son parámetros del propio FRAMEWORK
  console.log("POST /apitechu/update/:saldo/:account");
  var query = 'q={"IBAN":"'+ req.params.account +'"}';
  var new_balance=req.params.saldo
  console.log(query);
  console.log(new_balance);
  var httpClient=requestJson.createClient(baseMLabURL);
  console.log("Cliente HTTP creado");


  httpClient.get("account?" +query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg": "error"}
        res.status(500);
        res.send(response);
      }
      else{
          var putBody='{"$set":{"saldo":'+ new_balance +'}}';

          httpClient.put("account?" +query + "&" + mLabAPIKey,JSON.parse(putBody),
            function(err, resMLab, body){
              var response={"msg": "update OK", "balance": new_balance}
              res.send(response);
            });

      }

    })
  }

module.exports.BalanceUpdate=BalanceUpdate;

function BalanceUpdatev2(req, res) {
//req y res son parámetros del propio FRAMEWORK
  console.log("POST /apitechu/update/");
  var query = 'q={"IBAN":"'+ req.body.cuenta +'"}';
  var new_balance=parseInt(req.body.saldo);
  console.log(query);
  console.log(new_balance);
  var httpClient=requestJson.createClient(baseMLabURL);
  console.log("Cliente HTTP creado");


  httpClient.get("account?" +query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg": "error"}
        res.status(500);
        res.send(response);
      }
      else{
          var putBody='{"$set":{"saldo":'+ new_balance +'}}';

          httpClient.put("account?" +query + "&" + mLabAPIKey,JSON.parse(putBody),
            function(err, resMLab, body){
              var response={"msg": "update OK", "balance": new_balance}
              res.send(response);
            });

      }

    })
  }

module.exports.BalanceUpdatev2=BalanceUpdatev2;

  function getAccountById(req, res) {
  //req y res son parámetros del propio FRAMEWORK
    console.log("GET /apitechu/v1/accounts/:id");

    var id = req.params.id;
    var query = 'q={"id":'+ id +'}'

    console.log("La consulta es "+query);

    var httpClient=requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("account?" +query + "&" + mLabAPIKey,
      function(err, resMLab, body){


        if (err){
          var response = {"msg": "error"}
          res.status(500);
        }
        else{
          if(body.length >0){
            var response=body;
          }
          else{
            var response = {"msg": "user not found"};
            res.status(404);
          }
        }
        res.send(response);
      }
    );


}

module.exports.getAccountById=getAccountById;
