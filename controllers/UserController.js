
const io=require('../io');
const crypt=require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmg11ed/collections/";
//const mLabAPIKey = "apiKey=5zTFmFoXB8txjvfwTLCXt6mCqPL-P_ea";

const mLabAPIKey="apiKey="+ process.env.MLAB_API_KEY

function getUser(req, res) {
//req y res son parámetros del propio FRAMEWORK
  console.log("GET /apitechu/v1/user");
  //res.sendFile('MOCK_DATA.json', {root: __dirname});
  var users = require('../log.json');
  //res.send(users);
  res.send({"key": "Hola"})

}

function getUsersV1(req, res) {
//req y res son parámetros del propio FRAMEWORK
  console.log("GET /apitechu/v1/users");
  //res.sendFile('MOCK_DATA.json', {root: __dirname});
  var users = require('../MOCK_DATA.json');
  res.send(users);
  //res.send({"key": "Hola"})

}

function getUsersV2(req, res) {
//req y res son parámetros del propio FRAMEWORK
  console.log("GET /apitechu/v2/users");

  var httpClient=requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {"msg": "error"}
      res.send(response);
    }
  );
}

  function getUsersByIdV2(req, res) {
  //req y res son parámetros del propio FRAMEWORK
    console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id":'+ id +'}'

    console.log("La consulta es "+query);

    var httpClient=requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("user?" +query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        if (err){
          var response = {"msg": "error"}
          res.status(500);
        }
        else{
          if(body.length >0){
            var response=body[0];
          }
          else{
            var response = {"msg": "user not found"};
            res.status(404);
          }
        }
        res.send(response);
      }
    );


}

function createUserv1(req, res){
      console.log("POST /apitechu/v1/users");
      console.log(req.query.value);
      console.log(req.body.value);
      var newUser_old = {
        "first_name": req.query.first_name,
        "last_name": req.query.last_name,
        "email": req.query.value
      }
      var newUser = {
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.value
      }
      var users=require('../MOCK_DATA.json');
      users.push(newUser);

      io.writeUserDataToFile(users);

      console.log("Proceso de creación de usuario terminado");
      res.send({"msg": "usuario creado"});


}

function createUserv2(req, res) {
//req y res son parámetros del propio FRAMEWORK
  console.log("POST /apitechu/v2/users");
  newUser = req.body;

  console.log("last_name: " + newUser.last_name);
  console.log(newUser);

  newUser.password= crypt.hash(req.body.password)

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body){
      console.log("Usuario guardado");
      res.status(201).send({"msg": "User created"})
    }
)


}


function deleteUserv1(req, res){
      console.log("DELETE /apitechu/v1/users");
      console.log("Id usuario a borrar es: "+req.params.id);
      var users=require('../MOCK_DATA.json');
      var index=0;

      for (var i = 0, len = users.length; i < len; i++) {
        item=users[i];
        for (const prop in item) {
          if (item[prop]==req.params.id){
            index=i;
          }

        }

      }
      console.log(index)
      users.splice(index,1)
      //console.log(users)
      io.writeUserDataToFile(users);
      console.log("Proceso de borrado de usuario terminado");
      res.send({"msg": "usuario borrado"});
  }

//delete user using MLAB API
function deleteUserv2(req, res){
      console.log("DELETE /apitechu/v2/users/:id");

      var query = 'q={"id":'+ req.params.id +'}'

      console.log("La consulta es "+query);
      var bodyput = []
      var httpClient = requestJson.createClient(baseMLabURL);
      httpClient.put("user?" +query + "&" + mLabAPIKey,bodyput,
        function(err, resMLab, body){
//          console.log("Usuario borrado");
//          res.status(200).send({"msg": "User deleted"});
          if (err){
              var response = {"msg": "error"}
              res.status(500);
              res.send(response);
            }
          else {
            if(body.removed > 0){
              res.status(200).send({"msg": "User deleted"});
            }
            else{
              var response = {"msg": "user not found"};
              res.status(404);
              res.send(response);
            }
          }

        });
}


module.exports.deleteUserv1=deleteUserv1;
module.exports.deleteUserv2=deleteUserv2;
module.exports.createUserv1=createUserv1;
module.exports.createUserv2=createUserv2;
module.exports.getUsersV1=getUsersV1;
module.exports.getUsersV2=getUsersV2;
module.exports.getUsersByIdV2=getUsersByIdV2;
module.exports.getUser=getUser;
