
const io=require('../io');
const requestJson = require('request-json');


function getMovsV1(req, res){
      console.log("Get /apitechu/movs/:account");
      console.log("movimientos de cuenta a consultar: "+req.params.account);
      var users=require('../movimientos.json');
      var index=0;
      var movimientos_cuenta=[];
      for (var i = 0, len = users.length; i < len; i++) {
        item=users[i];
        for (const prop in item) {
          if (item[prop]==req.params.account){
            movimientos_cuenta.push(item);
          }

        }

      }
      console.log(movimientos_cuenta);
      res.send(movimientos_cuenta);
  }

  function getMovsV2(req, res){
        console.log("Get /apitechu/movs/");
        console.log("movimientos de cuenta a consultar: "+req.body.account);
        var users=require('../movimientos.json');
        var index=0;
        var movimientos_cuenta=[];
        for (var i = 0, len = users.length; i < len; i++) {
          item=users[i];
          for (const prop in item) {
            if (item[prop]==req.body.account){
              movimientos_cuenta.push(item);
            }

          }

        }
        console.log(movimientos_cuenta);
        res.send(movimientos_cuenta);
    }

function createMovsv1(req, res){
      console.log("POST /apitechu/movs");
      var ts = new Date();
      console.log(ts);
      var newMov = {
        "IBAN": req.body.IBAN,
        "ibalance": req.body.ibalance,
        "tipomov": req.body.tipomov,
        "importe": req.body.importe,
        "fbalance": req.body.fbalance,
        "timestamp": ts
      }
      console.log(newMov);
      var movimientos=require('../movimientos.json');
      movimientos.push(newMov);

      io.writeDataToFile(movimientos);

      console.log("Proceso de creación de movimientos terminado");
      res.send({"msg": "movimiento creado"});


}

module.exports.createMovsv1=createMovsv1;
module.exports.getMovsV1=getMovsV1;
module.exports.getMovsV2=getMovsV2;
