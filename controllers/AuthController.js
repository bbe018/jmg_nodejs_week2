const io=require('../io');
const crypt=require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmg11ed/collections/";
//const mLabAPIKey = "apiKey=5zTFmFoXB8txjvfwTLCXt6mCqPL-P_ea";

const mLabAPIKey="apiKey="+ process.env.MLAB_API_KEY

function loginCheck(req, res) {
//req y res son parámetros del propio FRAMEWORK
  console.log("POST /apitechu/v1/login");
  //res.sendFile('MOCK_DATA.json', {root: __dirname});
  var user = req.body

  var users=require('../log.json');
  var index=0
  for (var i = 0, len = users.length; i < len; i++) {
    item=users[i];
    if (item.email == req.body.email && item.passw == req.body.passw){
      users[i].logged= true
      index=i;
    }

  }

  io.writeUserDataToFile(users);
  res.send({"msg": "OK", "id":users[index].id})
}
module.exports.loginCheck=loginCheck;

function logout(req, res) {
//req y res son parámetros del propio FRAMEWORK
  console.log("POST /apitechu/v1/logout/id");
  //res.sendFile('MOCK_DATA.json', {root: __dirname});
  var user = req.params.id


    var users=require('../log.json');
    var index=0
    for (var i = 0, len = users.length; i < len; i++) {
      item=users[i];
      if (item.id == req.params.id){
        delete users[i].logged
        index=i;
      }

    }
res.send({"msg": "Logout correcto", "id":users[index].id})
}
module.exports.logout=logout;

function loginCheckv2(req, res) {
//req y res son parámetros del propio FRAMEWORK
  console.log("POST /apitechu/v2/login");
  var email = req.body.email;
  var query = 'q={"email":"'+ email +'"}';
  var plainPwd = req.body.password;

  var httpClient=requestJson.createClient(baseMLabURL);
  console.log("Cliente HTTP creado");
  console.log(plainPwd);

  httpClient.get("user?" +query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg": "error"}
        res.status(500);
        res.send(response);
      }
      else{
        if(crypt.checkPassword(plainPwd,body[0].password)){
//        if(true){
          console.log(body[0]);
          var id = body[0].id
          var putBody='{"$set":{"logged":true}}';
          httpClient.put("user?" +query + "&" + mLabAPIKey,JSON.parse(putBody),
            function(err, resMLab, body){
              var response={"msg": "login OK", "id": id}
              res.send(response);
            });

        }
        else{
          var response = {"msg": "user not found"};
          res.status(404);
          res.send(response);
        }
      }

    })
  }

module.exports.loginCheckv2=loginCheckv2;


function logoutv2(req, res) {
//req y res son parámetros del propio FRAMEWORK
  console.log("POST /apitechu/v2/logout/id");
  //res.sendFile('MOCK_DATA.json', {root: __dirname});
  var user = req.params.id
  var query = 'q={"id":'+ user +'}';

  var httpClient=requestJson.createClient(baseMLabURL);
  console.log("Cliente HTTP creado");

  httpClient.get("user?" +user + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg": "error"}
        res.status(500);
        res.send(response);
      }
      else{
        if(body.length>0){
//        if(true){
          var putBody='{"$unset":{"logged":""}}';
          httpClient.put("user?" +query + "&" + mLabAPIKey,JSON.parse(putBody),
            function(err, resMLab, body){
              var response={"msg": "logout OK", "id": user}
              res.send(response);
            });

        }
        else{
          var response = {"msg": "user not found"};
          res.status(404);
          res.send(response);
        }
      }

    })

}
module.exports.logoutv2=logoutv2;
